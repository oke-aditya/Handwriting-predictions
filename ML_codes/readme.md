This folder contains all the ML models used in this implementation.
We have used first Object detection for breaking the sentence into words.
Thanks to github and the credits are mentioned in the object detection code for providing the algorithm and the implementation.

Then we have broken the words into digits using Object detection again.

We have used a CNN model to classify the handwritten digits.
Finally an SVM implementation to decide how goood or bad a digit is.

NOTE:- The models are not integrated still we will be doing it soon.
        Also the ML models need to be saved in a .h5 or .pkl or .json file format. The code to do the same is also added please use
        it while you are deploying the codes.
